CORS_URLS_REGEX = '/api/' # Cors headers enabled

# Request from
CORS_ORIGIN_WHITELIST = [
    "http://192.168.0.11:8100",
    'http://localhost:8100',
]
#CORS_ORIGIN_ALLOW_ALL = True

from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = list(default_headers) + [
    'my-custom-header',
]