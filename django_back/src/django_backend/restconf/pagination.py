from rest_framework import pagination


class GlobalAPIPagination(pagination.LimitOffsetPagination):
	default_limit 	= 10
	max_limit		= 10