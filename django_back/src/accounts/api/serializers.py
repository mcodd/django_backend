from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.utils import timezone
import datetime

from rest_framework_jwt.settings import api_settings

from profiles.models import Profile

jwt_payload_handler 			= api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler 				= api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler 	= api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
expire_delta 					= api_settings.JWT_REFRESH_EXPIRATION_DELTA

User = get_user_model()


class PinCheckSerializer(serializers.Serializer):
	# auth_pin	 		= serializers.CharField(write_only=True)

	def validate_auth_pin(self, value):
		qs = Profile.objects.filter(username__iexact=self.context['username'])
		if qs.exists():
			profile = qs.first()
			if profile.auth_pin != value:
				raise serializers.ValidationError({'detail': 'Pin incorrecto.'})
			return value
		else:
			raise serializers.ValidationError({'detail': 'Solo usuarios autorizados.'})

	def create(self, validated_data):
		return validated_data


class SetPinSerializer(serializers.Serializer):
	# auth_pin	 		= serializers.CharField(write_only=True)
	
	def validate_auth_pin(self, value):
		pin = str(value)
		if len(pin) > 4:
			raise serializers.ValidationError({'detail': 'Pin invalido.'})
		elif pin.isdigit() == False:
			raise serializers.ValidationError({'detail': 'Pin invalido.'})
		return value

	def create(self, validated_data):
		return validated_data


class SetPasswordSerializer(serializers.Serializer):
	# username 		= serializers.CharField(write_only=True)
	# password	 	= serializers.CharField(style={'input_type': 'password'}, write_only=True)
	# password2	 	= serializers.CharField(style={'input_type': 'password'}, write_only=True)

	def validate_username(self, value):
		qs = User.objects.filter(username__iexact=value)
		if qs.exists():
			raise serializers.ValidationError({'detail': 'Nombre de usuario ya existe.'})
		return value

	def validate(self, data):
		pw = data.get('password')
		pw2 = data.get('password2')
		if pw != pw2:
			raise serializers.ValidationError({'detail': 'Contraseñas deben coincidir.'})
		return data

	def create(self, validated_data):
		return validated_data


class SignupSerializer(serializers.Serializer):
	# identity 		= serializers.CharField(write_only=True)
	# first_name	 	= serializers.CharField(write_only=True)
	# last_name	 	= serializers.CharField(write_only=True)
	# phone			= serializers.CharField(write_only=True)
	# email	 		= serializers.EmailField(write_only=True)

	def validate_phone(self, value):
		phone = str(value)
		if phone.isdigit() == False:
			raise serializers.ValidationError({'detail': 'Numero de Telefono incorrecto.'})
		elif len(phone) > 10:
			raise serializers.ValidationError({'detail': 'Numero de Telefono demasiado largo.'})
		return value

	def validate_identity(self, value):
		qs = Profile.objects.filter(identity__iexact=value)
		if qs.exists():
			raise serializers.ValidationError({'detail': 'Cédula/Ruc ya existe.'})
		return value

	def validate_email(self, value):
		qs = Profile.objects.filter(email__iexact=value)
		if qs.exists():
			raise serializers.ValidationError({'detail': 'Email ya existe.'})
		return value

	def create(self, validated_data):
		return validated_data


class RegisterSerializer(serializers.ModelSerializer):
	identity 		= serializers.CharField(write_only=True)
	username	 	= serializers.CharField(write_only=True)
	first_name	 	= serializers.CharField(write_only=True)
	last_name	 	= serializers.CharField(write_only=True)
	phone 			= serializers.CharField(write_only=True)
	email	 		= serializers.CharField(write_only=True)
	auth_pin 		= serializers.CharField(write_only=True)
	password	 	= serializers.CharField(style={'input_type': 'password'}, write_only=True)
	token 			= serializers.SerializerMethodField(read_only=True)
	expires 		= serializers.SerializerMethodField(read_only=True)
	success			= serializers.SerializerMethodField(read_only=True)
	

	class Meta:
		model = Profile
		fields = [
			'identity',
			'username',
			'first_name',
			'last_name',
			'phone',
			'email',
			'auth_pin',
			'password',
			'token',
			'expires',
			'success'
		]

	def get_success(self, obj):
		message = "Gracias por registrarse. Por favor verifique su email."
		return message

	def get_token(self, obj):
		user = obj
		payload = jwt_payload_handler(user)
		token = jwt_encode_handler(payload)
		return token

	def get_expires(self, obj):
		return timezone.now() + expire_delta - datetime.timedelta(seconds=200)

	def create(self, validated_data):
		user_obj = User(
			username = validated_data.get('username'),
			email = validated_data.get('email'),
			first_name = validated_data.get('first_name'),
			last_name = validated_data.get('last_name'),
		)
		
		user_obj.set_password(validated_data.get('password'))
		user_obj.is_active = True
		user_obj.save()

		prof_obj = Profile.objects.create(
			username = validated_data.get('username'),
			email = validated_data.get('email'),
			first_name = validated_data.get('first_name'),
			last_name = validated_data.get('last_name'),
			identity = validated_data.get('identity'),
			phone = validated_data.get('phone'),
			auth_pin = validated_data.get('auth_pin')
		)
		
		prof_obj.save()


		return user_obj


