from django.urls import path

from .views import(
	AuthAPIView,
	PinCheckAPIView,
	RegisterAPIView, 
	SetpasswordAPIView, 
	SetpinAPIView, 
	SignupAPIView,
	)

urlpatterns = [
	path('', AuthAPIView.as_view()),
	path('pincheck/', PinCheckAPIView.as_view()),
	path('register/', RegisterAPIView.as_view()),
	path('signup/', SignupAPIView.as_view()),
	path('setpass/', SetpasswordAPIView.as_view()),
	path('setpin/', SetpinAPIView.as_view()),
	
]