from django.contrib.auth import authenticate, get_user_model
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Q

from .serializers import ( 
	RegisterSerializer, 
	SignupSerializer, 
	SetPasswordSerializer, 
	SetPinSerializer,
	PinCheckSerializer
	)

from .permissions import AnonPermissionOnly

from profiles.models import Profile

from rest_framework_jwt.settings import api_settings

jwt_payload_handler 			= api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler 				= api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler 	= api_settings.JWT_RESPONSE_PAYLOAD_HANDLER

User = get_user_model()

class AuthAPIView(APIView):
	permission_classes 		= [AnonPermissionOnly]

	def post(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			return Response({'detail': 'Ya estas autenticado'}, status=400)
		data = request.data
		username = data.get('username') # username or email
		password = data.get('password')
		qs = User.objects.filter(
			Q(username__iexact=username)|
			Q(email__iexact=username)
		).distinct()
		if qs.count() == 1:
			user_obj = qs.first()
			if user_obj.check_password(password):
				user = user_obj
				payload = jwt_payload_handler(user)
				token = jwt_encode_handler(payload)
				response = jwt_response_payload_handler(token, user, request=request)
				return Response(response)
		return Response({'detail': 'Usuario/Contraseña incorrecta.'}, status=401)


class PinCheckAPIView(generics.CreateAPIView):
	queryset				= Profile.objects.all()
	serializer_class		= PinCheckSerializer
	permission_classes 		= [permissions.IsAuthenticated] 

	def get_serializer_context(self, *args, **kwargs):
		return {'username': self.request.user.username}


class RegisterAPIView(generics.CreateAPIView):
	queryset				= Profile.objects.all()
	serializer_class		= RegisterSerializer
	permission_classes 		= [AnonPermissionOnly] 


class SignupAPIView(generics.CreateAPIView):
	queryset				= Profile.objects.all()
	serializer_class		= SignupSerializer
	permission_classes 		= [AnonPermissionOnly] 


class SetpasswordAPIView(generics.CreateAPIView):
	queryset				= User.objects.all()
	serializer_class		= SetPasswordSerializer
	permission_classes 		= [AnonPermissionOnly] 


class SetpinAPIView(generics.CreateAPIView):
	queryset				= Profile.objects.all()
	serializer_class		= SetPinSerializer
	permission_classes 		= [AnonPermissionOnly] 






