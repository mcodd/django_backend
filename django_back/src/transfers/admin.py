from django.contrib import admin

from .models import Transfer

# Register your models here.
class TransferAdmin(admin.ModelAdmin):
	list_display = ['from_user', 'receiver_user', 'amount', 'timestamp']
	class Meta:
		model = Transfer

admin.site.register(Transfer, TransferAdmin)