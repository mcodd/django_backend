from django.db.models.signals import post_save
from django.db import models

from profiles.models import Profile

# Create your models here.
class Transfer(models.Model):
	from_user		= models.ForeignKey(Profile, on_delete=models.CASCADE)
	receiver_user	= models.CharField(blank=True, null=True, max_length=120)
	amount			= models.DecimalField(max_digits=7, decimal_places=2)
	timestamp		= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.from_user)


def post_save_update_balances(sender, created, instance, **kwargs):
	if created:
		sender_profile = Profile.objects.get(username__iexact=instance.from_user)
		username = instance.receiver_user
		receiver_profile = Profile.objects.get(username__iexact=username)
		receiver_profile.balance = receiver_profile.balance + instance.amount
		sender_profile.balance = sender_profile.balance - instance.amount
	
		sender_profile.save()
		receiver_profile.save()


post_save.connect(post_save_update_balances, sender=Transfer)