from rest_framework import serializers

from profiles.models import Profile
from transfers.models import Transfer


class TransferSerializer(serializers.ModelSerializer):
	receiver_user 	= serializers.CharField(write_only=True)
	amount	 		= serializers.DecimalField(write_only=True, max_digits=7, decimal_places=2)

	class Meta:
		model = Transfer
		fields = [
			'receiver_user',
			'amount',
			]

	def validate_receiver_user(self, value):
		qs = Profile.objects.filter(username__iexact=value)
		if qs.exists():
			return qs.first().username
		else:
			raise serializers.ValidationError("Usuario no existe.")


	def validate_amount(self, value):
		qs = Profile.objects.filter(username__iexact=self.context['username'])
		if qs.exists():
			profile = qs.first()
			balance = profile.balance
			if value > balance:
				raise serializers.ValidationError('Monto debe ser menor o igual al balance')
			return value
		else:
			raise serializers.ValidationError('Solo usuarios autorizados.')

	def create(self, validated_data):
		qs = Profile.objects.filter(username__iexact=self.context['username'])
		if qs.exists():
			profile_obj = qs.first()
		
			transfer_obj = Transfer.objects.create(
				from_user=profile_obj,
				receiver_user=validated_data.get('receiver_user'),
				amount=validated_data.get('amount')
				)

			transfer_obj.save()

			return transfer_obj
		else:
			raise serializers.ValidationError('Solo usuarios autorizados.')

			