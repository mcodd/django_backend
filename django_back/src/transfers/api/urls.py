from django.urls import path

from .views import TransferCreateAPIView

urlpatterns = [
    path('', TransferCreateAPIView.as_view()),
    
]