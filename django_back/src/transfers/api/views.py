from rest_framework import generics, permissions

from transfers.models import Transfer
from .serializers import TransferSerializer


class TransferCreateAPIView(generics.CreateAPIView):
	permission_class 		= [permissions.IsAuthenticated] # must be logged in
	serializer_class		= TransferSerializer
	queryset 				= Transfer.objects.all()

	def get_serializer_context(self, *args, **kwargs):
		return {'username': self.request.user.username}