from django.urls import path

from .views import PaymargoAPIView

urlpatterns = [
    path('', PaymargoAPIView.as_view()),
    
]