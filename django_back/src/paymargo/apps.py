from django.apps import AppConfig


class PaymargoConfig(AppConfig):
    name = 'paymargo'
