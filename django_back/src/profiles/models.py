import os
from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save


def get_filename_ext(filepath):
	basename = os.path.basename(filepath)
	name, ext = os.path.splitext(basename)
	return name, ext


def upload_image_path(instance, filename):
	print(instance)
	new_filename = instance.username
	name, ext = get_filename_ext(filename)
	final_filename = f'{new_filename}{ext}'  # f' solo se usa para django 1.11
	return f'images/{new_filename}/{final_filename}'


class ProfileQuerySet(models.QuerySet):
	def active(self):
		return self.filter(active=True)


class ProfileManager(models.Manager):
	def get_queryset(self):
		return ProfileQuerySet(self.model, using=self._db)

	def all(self):
		return self.get_queryset().active()


# Create your models here.
class Profile(models.Model):
	username		= models.CharField(blank=True, null=True, max_length=20)
	first_name		= models.CharField(blank=True, null=True, max_length=20)
	last_name		= models.CharField(blank=True, null=True, max_length=20)
	email			= models.EmailField(blank=True, null=True)
	password 		= models.CharField(blank=True, null=True, max_length=120)	
	auth_pin		= models.CharField(blank=True, null=True, max_length=4)
	identity		= models.CharField(blank=True, null=True, max_length=20)
	phone			= models.CharField(blank=True, null=True, max_length=20)
	balance			= models.DecimalField(default=0.00, decimal_places=2, max_digits=7)
	account_type 	= models.CharField(default='Personal', max_length=20)
	image			= models.ImageField(default='images/default_avatar.jpg', upload_to=upload_image_path, blank=True, null=True)
	active      	= models.BooleanField(default=True)
	timestamp		= models.DateTimeField(auto_now_add=True)


	objects = ProfileManager()

	def __str__(self):
		return self.username

	class Meta:
		ordering = ['username']
		verbose_name = 'Profile'
		verbose_name_plural = 'Profiles'


def pre_save_profile_handler(sender, instance, **kwargs):
	ident = instance.identity
	if ident.endswith('001'):
		instance.account_type = 'Business'


pre_save.connect(pre_save_profile_handler, sender=Profile)