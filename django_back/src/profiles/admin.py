from django.contrib import admin

from .models import Profile

class ProfileAdmin(admin.ModelAdmin):
	list_display = ['username', 'first_name', 'last_name', 'account_type']
	class Meta:
		model = Profile

# Register your models here.
admin.site.register(Profile, ProfileAdmin)