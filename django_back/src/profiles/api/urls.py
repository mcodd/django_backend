from django.urls import path

from .views import ProfileAPIView, MemberAPIView # EditProfileAPIView

urlpatterns = [
	path('', MemberAPIView.as_view()),
    path('<str:username>', ProfileAPIView.as_view()),
    # path('<str:username>/edit', EditProfileAPIView.as_view())
]