from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView


from .serializers import ProfileSerializer, MemberSerializer
from profiles.models import Profile



class ProfileAPIView(generics.RetrieveAPIView):
	permissions			= [permissions.IsAuthenticated]
	queryset 			= Profile.objects.all()
	serializer_class 	= ProfileSerializer
	lookup_field		= 'username' 


class MemberAPIView(generics.ListAPIView):
	permission_class 		= [permissions.IsAuthenticated] # must be logged in
	serializer_class		= MemberSerializer
	search_fields			= ('first_name', 'last_name', 'username')
	queryset 				= Profile.objects.all()


# class EditProfileAPIView(generics.RetrieveUpdateAPIView):
# 	permissions			= [permissions.IsAuthenticated]
# 	queryset 			= User.objects.all()
# 	serializer_class 	= EditProfileSerializer
# 	lookup_field		= 'username'

