# from django.contrib.auth import get_user_model
from rest_framework import serializers

from profiles.models import Profile

# User = get_user_model()


class ProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = [
			'username',
			'first_name',
			'last_name',
			'account_type',
			'balance',
			'phone',
			'image'
		]
		read_only_fields = [
			'username',
			'first_name',
			'last_name',
			'account_type',
			'balance',
			'phone',
			'image'
		]


class MemberSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = [
			'first_name',
			'last_name',
			'username',
			'account_type',
			'active',
			'image'
		]

		read_only_fields = [
			'first_name',
			'last_name',
			'username',
			'account_type',
			'active',
			'image'
		]

	def validate_username(self, value):
		if len(value) > 100:
			raise serializers.ValidationError("Nombre de ususario es muy largo.")
		return value


# class EditProfileSerializer(serializers.ModelSerializer):
# 	profile = ProfileSerializer()

# 	class Meta:
# 		model = User
# 		fields = [
# 			'first_name',
# 			'last_name',
# 			'email',
# 			'profile'
# 		]
